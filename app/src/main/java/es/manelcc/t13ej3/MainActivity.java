package es.manelcc.t13ej3;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private static final int TAKE_PHOTO_CODE = 1;
    Button btnFoto;
    ImageView img;
    Uri fileLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnFoto = (Button) findViewById(R.id.bnFoto);
        img = (ImageView) findViewById(R.id.foto);

        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeFoto();
            }
        });
    }

    private void takeFoto() {
        File f = null;
        try {
            f = getTempFile(this);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            fileLocation = Uri.fromFile(f);
            startActivityForResult(intent, TAKE_PHOTO_CODE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File getTempFile(Context context) throws IOException {
//Creamos la ruta donde guardaremos la foto
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFile = timeStamp + "_";
        File image = File.createTempFile(imageFile, ".jpg",
                Environment.getExternalStorageDirectory());
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case TAKE_PHOTO_CODE:
                    //final File file = getTempFile(this);
                    try {

                        img.setImageURI(fileLocation);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
